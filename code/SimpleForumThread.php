<?php

class SimpleForumThread extends DataObject {
	
	static $has_many = array (
		'SimpleForumPosts' => 'SimpleForumPost'
	);
	
	
	private static $db = array(
		"Title" => "Varchar(255)",
		"NumViews" => "Int"		
	);
	
	private static $has_one = array(		
		"SimpleForumPage" => "SimpleForumPage" 
	);
	
	private static $many_many = array(		
		"SimpleForumTags" => "SimpleForumTag"
	);
	
	public function isSubscribed(){
		$threadID = $this->ID;
		if (DataObject::get('SimpleForumSubscription',"MemberID = '". Member::currentUser()->ID."' AND SimpleForumThreadID = '".$threadID."'")->count() > 0) { return true;}
	
	}
	
	
	
						   
}



