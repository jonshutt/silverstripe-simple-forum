<?php
class SimpleForumPage extends Page  {

	

	private static $has_one = array(
	);
	
	static $has_many = array (
		'SimpleForumThreads' => 'SimpleForumThread'
	);
	
	
	function getCMSFields() { 
		$fields = parent::getCMSFields();		
		
		return $fields; 

	}

}


class SimpleForumPage_Controller extends Page_Controller {


	private static $allowed_actions = array (
		'thread',
		'ask',		
		'subscribe',
		'unsubscribe',
		'askForm',
		'answerForm',
		'migrate'
	);
	
	public function GetSimpleForumThread(){
		if($this->request->param('Action')=='thread'){
			if($threadID = $this->request->param('ID')){			
				return DataObject::get_by_id('SimpleForumThread',  $threadID);						
			}
		}
		
	}
	
	
	public function answerForm(){
		// Create fields
		$threadID= $this->request->param('ID');
		
		$fields = new FieldList(
			new TextareaField('Content'),	
			new HiddenField('SimpleForumThreadID', '',$threadID)			
		);
		
		$validator = new RequiredFields('Content');

		// Create actions
		$actions = new FieldList(
			new FormAction('doAnswerForm', 'Post Answer')
		);

		return new Form($this, 'answerForm', $fields, $actions);
		
	}
	
	public function doAnswerForm($data, $form) {
            $post = new SimpleForumPost();
            $form->saveInto($post);
            $post->AuthorID = Member::currentUser()->ID;
		    $post->write();
            return $this->redirectBack();
    }
	
	
	public function askForm(){
		// Create fields
		$threadID= $this->request->param('ID');
		
		$fields = new FieldList(
			new TextField('Title'),	
			new TextareaField('Content')			
		);
		
		$validator = new RequiredFields('Question','Content');

		// Create actions
		$actions = new FieldList(
			new FormAction('doAskForm', 'Post Question')
		);

		return new Form($this, 'askForm', $fields, $actions);
	}
	
	
	public function doAskForm($data, $form) {
            
		$thread = new SimpleForumThread();
		$thread->Title =$data['Title'];
		$thread->SimpleForumPageID = $this->ID;
		$thread->write();
		
		$post = new SimpleForumPost();
		
		$post->AuthorID = Member::currentUser()->ID;
		$post->Content = $data['Content'];
		$post->SimpleForumThreadID = $thread->ID;
		$post->write();
		
		$subscription = new SimpleForumSubscription();
		$subscription->MemberID =  Member::currentUser()->ID;
		$subscription->SimpleForumThreadID = $thread->ID; 
		$subscription->write();
		
		return $this->redirect($this->Link().'thread/'.$thread->ID);
    }
	
	
	public function subscribe(){
		
		if ($threadID= $this->request->param('ID')) {
			$subscription = new SimpleForumSubscription();
			$subscription->MemberID =  Member::currentUser()->ID;
			$subscription->SimpleForumThreadID = $threadID; 
			$subscription->write();
		}	
		return $this->redirectBack();	
		
	}
	
	public function unsubscribe(){
		if ($threadID= $this->request->param('ID')) {
			
			$subscription = DataObject::get('SimpleForumSubscription',"MemberID = '". Member::currentUser()->ID."' AND SimpleForumThreadID = '".$threadID."'", );
			$subscription->delete();
		}
		return $this->redirectBack();
	}
	
	public function migrate(){
		$threads = 	DataObject::get('ForumThread','', 'LastEdited DESC'); 
		
		foreach($threads as $thread){
			$posts = $thread->Posts();
			
			// create tag
			
			$forumTitle = $thread->Forum()->Title;
			
			if (DataObject::get('SimpleForumTag',"Title = '$forumTitle' ")->Count() > 0) { 
				$SimpleForumTag  = DataObject::get('SimpleForumTag',"Title = '$forumTitle' ")->first();
			} else {
				print 'creating new tag >> ';
				$SimpleForumTag = new SimpleForumTag;
				$SimpleForumTag->Title = $forumTitle;
				$SimpleForumTag->write();
			}
			
			$tagID = $SimpleForumTag->ID;
			
			
			// create thread
			$newthread = new SimpleForumThread;
			$newthread->Title = $thread->Title;
			$newthread->NumViews = $thread->NumViews;
			$newthread->SimpleForumPageID = $this->ID;
			$newthread->write();
			
			$newthread->SimpleForumTags()->add($SimpleForumTag);			
			
			foreach($posts as $post) {
				print $post->Content. '<hr />';
				$newpost = new SimpleForumPost;
				
				$newpost->Content = $post->Content;
				$newpost->AuthorID= $post->AuthorID;
				$newpost->SimpleForumThreadID= $newthread->ID;
				$newpost->write();
			}
		}
		
	}

}