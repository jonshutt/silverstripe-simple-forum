<div class="container">
	<div class="row top-bottom-buffer">
		
		<div class="col-md-3 ">
			<% include SideBar %>
		</div>
		
		
		<div class="col-md-9 ">
        
			<% loop GetSimpleForumThread %>
				
			
			<h1>$Title</h1>
			
			<% if CurrentMember %>
				<% if $isSubscribed %> 
					<p><a href="{$Top.Link}unsubscribe/$ID" class="btn btn-info">Unsubscribe</a></p>
				<% else %> 
					<p><a href="{$Top.Link}subscribe/$ID" class="btn btn-info">Subscribe</a></p>
				<% end_if %>
			<% end_if %>
			
				<div>
					<% loop SimpleForumPosts %>
						<h4>$Author.FirstName</h4>						
						$Content
					<% end_loop %>
					
				</div>
				
				
				
			<% end_loop %>
            
			
			 <% if CurrentMember %>
				$answerForm
			 
			<% else %>
				<p>Please log in to post a response.</p>
			<% end_if %>  
			
			
			
			
				
		</div>
	</div>
</div>